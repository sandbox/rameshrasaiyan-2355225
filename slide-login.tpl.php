<?php
/**
 * @file
 * Theme implementation to display the slide login.
 */
?>
<div id="slide-login--wrapper">
  <div class="slide-login--user-login-wrapper">
    <div class="slide-login--user-login">
      <?php
        global $user;
        if (!$user->uid && !(arg(0) == 'user' && !is_numeric(arg(1)))):
          $login_form = drupal_get_form('user_login_block');
        echo drupal_render($login_form);
      ?>
    </div>
  </div>
  <div class="slide-login--toggle-handler" style="border-top: 5px solid <?php print $bg_color; ?>">
    <a href="#" class="slide-login--toggle-open"><?php print $open; ?></a>
    <a href="#" class="slide-login--toggle-close"><?php print $close; ?></a>
  </div>
</div>
<?php endif; ?>
