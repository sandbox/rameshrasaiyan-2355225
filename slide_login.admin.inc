<?php
/**
 * @file
 * This fill contains all the required form elements for the admin.
 */

function slide_login_block($form, &$form_state) {
  $form = array();

  $form['slide_toggle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide Toggle Texts'),
  );

  $form['slide_toggle']['slide_open'] = array(
    '#type' => 'textfield',
    '#title' => t('Type your slide open text'),
    '#description' => t('by default it will be "Slide Login"'),
    '#default_value' => variable_get('slide_open', 'Slide Login'),
    '#required' => TRUE,
  );

  $form['slide_toggle']['slide_close'] = array(
    '#type' => 'textfield',
    '#title' => t('Type your slide close text'),
    '#description' => t('by default it will be "close"'),
    '#default_value' => variable_get('slide_close', 'Close'),
    '#required' => TRUE,
  );

  $form['slide_theme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme your slider'),
  );

  $form['slide_theme']['bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Paste your background color here'),
    '#description' => t('Use the hex color to change the background, hex color will be in this "#FFFFFF" format.'),
    '#default_value' => variable_get('bg_color', '#000000'),
    '#required' => TRUE,
  );

  $form['slide_theme']['bg_img'] = array(
    '#type' => 'textfield',
    '#title' => t('Paste your background image pattern here'),
    '#description' => t('Use the hex color to change the background image here, should be absolute URL and repeatable image'),
    '#default_value' => variable_get('bg_img', 'http://subtlepatterns.com/patterns/carbon_fibre.png'),
  );

  $form['slide_theme']['link_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Paste your link color here'),
    '#description' => t('Use the hex color to change the background, hex color will be in this "#FFFFFF" format.'),
    '#default_value' => variable_get('link_color', '#FFFFFF'),
    '#required' => TRUE,
  );

  $form['slide_theme']['text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text color'),
    '#description' => t('Use the hex color to change the background, hex color will be in this "#FFFFFF" format.'),
    '#default_value' => variable_get('text_color', '#999999'),
    '#required' => TRUE,
  );

  return (system_settings_form($form));
}
