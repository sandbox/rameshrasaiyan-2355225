(function ($) {
  Drupal.behaviors.slideLogin = {
    attach: function (context, settings) {
      $('.slide-login--toggle-open').click(function() {
        $('.slide-login--user-login').slideDown('fast');
      });

      $('.slide-login--toggle-close').click(function() {
        $('.slide-login--user-login').slideUp('slow');
      });

      $('.slide-login--toggle-handler a').click(function() {
        $('.slide-login--toggle-handler a').toggle();
      });
    }
  };
}(jQuery));